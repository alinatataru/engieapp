create table if not exists tab_users
(
    email        varchar(256) not null
    constraint tab_users_pk
    primary key,
    first_name   varchar(64),
    last_name    varchar(64),
    phone_number varchar(16),
    password     varchar(256) not null
    );

create table if not exists tab_invoices
(
    invoice_number varchar(12)  not null
    constraint invoice_pk
    primary key,
    create_date    date,
    due_date       date,
    amount         numeric,
    amount_paid    numeric,
    status         varchar(16),
    user_id        varchar(256) not null
    constraint tab_invoices_tab_users_email_fk
    references tab_users
    );

create table if not exists tab_addresses
(
    country       varchar(32),
    county        varchar(32),
    city          varchar(32),
    street_number varchar(4),
    postal_code   varchar(16),
    id            uuid not null
    constraint tab_addresses_pk
    primary key
    );

create table if not exists tab_users_addresses
(
    user_id    varchar(256)
    constraint tab_users_addresses_tab_users_email_fk
    references tab_users,
    address_id uuid not null
    constraint tab_users_addresses_pk
    primary key
    constraint tab_users_addresses_tab_addresses_id_fk
    references tab_addresses
    );

