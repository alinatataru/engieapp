open module com.unitbv.appfx {
	requires static lombok;
	requires static org.mapstruct;
	requires static lombok.mapstruct;
	requires static java.compiler;

	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.web;
	requires org.controlsfx.controls;
	requires com.dlsc.formsfx;
	requires validatorfx;
	requires org.kordamp.ikonli.javafx;
	requires org.kordamp.bootstrapfx.core;
	requires eu.hansolo.tilesfx;

	requires eclipselink;
	requires jakarta.persistence;
	requires jakarta.activation;
	requires jakarta.xml.bind;

	exports com.unitbv.appfx.address to eclipselink;
	exports com.unitbv.appfx.invoice to eclipselink;
	exports com.unitbv.appfx.user to eclipselink;

	exports com.unitbv.appfx;
	exports com.unitbv.appfx.config.persistence;
	exports com.unitbv.appfx.user.transport to eclipselink;
	exports com.unitbv.appfx.config.validation;
	exports com.unitbv.appfx.presentation;
}



