package com.unitbv.appfx.presentation;

import com.unitbv.appfx.address.transport.AddressDto;
import com.unitbv.appfx.config.validation.ValidationHelper;
import com.unitbv.appfx.user.UserDao;
import com.unitbv.appfx.user.UserService;
import com.unitbv.appfx.user.transport.UserDto;
import com.unitbv.appfx.user.transport.UserDtoMapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import org.mapstruct.factory.Mappers;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

@Log
public class RegisterAccountController {
	private Stage stage;
	private Scene scene;
	private Parent root;

	private final UserDtoMapper userDtoMapper = Mappers.getMapper(UserDtoMapper.class);
	private final UserService userService = new UserService(new UserDao());

	@FXML
	TextField firstName;
	@FXML
	Label errorFirstNameLabel;
	@FXML
	TextField lastName;
	@FXML
	Label errorLastNameLabel;
	@FXML
	TextField email;
	@FXML
	Label errorEmailLabel;
	@FXML
	TextField password;
	@FXML
	Label errorPasswordLabel;
	@FXML
	TextField phoneNumber;
	@FXML
	Label errorPhoneNumberLabel;
	@FXML
	TextField codInst;
	@FXML
	Label errorCodInstLabel;
	@FXML
	TextField codContr;
	@FXML
	Label errorCodContrLabel;
	@FXML
	TextField country;
	@FXML
	Label errorCountryLabel;
	@FXML
	TextField county;
	@FXML
	Label errorCountyLabel;
	@FXML
	TextField city;
	@FXML
	Label errorCityLabel;
	@FXML
	TextField streetNumber;
	@FXML
	Label errorStreetNumberLabel;
	@FXML
	TextField postalCode;
	@FXML
	Label errorPostalCodeLabel;
	@FXML
	Button backToLogIn;

	public void switchToLogin(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("login.fxml")));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void register(ActionEvent event) throws IOException {

		if (invalidForm()) {
			return;
		}

		var addressDto = AddressDto.builder()
				.country(country.getText())
				.county(county.getText())
				.city(city.getText())
				.streetNumber(streetNumber.getText())
				.postalCode(postalCode.getText())
				.build();

		var userDto = UserDto.builder()
				.firstName(firstName.getText())
				.lastName(lastName.getText())
				.email(email.getText())
				.password(password.getText())
				.phoneNumber(phoneNumber.getText())
				.addresses(Set.of(addressDto))
				.build();

		userService.save(userDtoMapper.from(userDto));


		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText("Register Account");
		alert.setContentText("Operation Successful!");
		alert.showAndWait();

		switchToLogin(event);

	}

	private boolean invalidForm() {
		return Stream.of(ValidationHelper.applyValidation(email, errorEmailLabel, ValidationHelper.EMAIL_NOT_VALID, ValidationHelper.isEmailValid)
				, ValidationHelper.applyValidation(phoneNumber, errorPhoneNumberLabel, ValidationHelper.PHONE_NUMBER_NOT_VALID, ValidationHelper.isPhoneNumberValid)
				, ValidationHelper.applyValidation(password, errorPasswordLabel, ValidationHelper.PASSWORD_NOT_VALID, ValidationHelper.isPasswordValid)
				, ValidationHelper.applyValidation(firstName, errorFirstNameLabel, ValidationHelper.FIRST_NAME_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(lastName, errorLastNameLabel, ValidationHelper.LAST_NAME_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(codInst, errorCodInstLabel, ValidationHelper.COD_INST_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(codContr, errorCodContrLabel, ValidationHelper.COD_CONTRACT_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(country, errorCountryLabel, ValidationHelper.COUNTRY_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(county, errorCountyLabel, ValidationHelper.COUNTY_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(city, errorCityLabel, ValidationHelper.CITY_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(streetNumber, errorStreetNumberLabel, ValidationHelper.STREET_NUMBER_NOT_VALID, ValidationHelper.isNotEmpty)
				, ValidationHelper.applyValidation(postalCode, errorPostalCodeLabel, ValidationHelper.POSTAL_CODE_NOT_VALID, ValidationHelper.isNotEmpty)
		).anyMatch(validationResult -> !validationResult);
	}
}
