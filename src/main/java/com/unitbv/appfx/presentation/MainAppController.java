package com.unitbv.appfx.presentation;

import com.unitbv.appfx.invoice.Invoice;
import com.unitbv.appfx.session.UserSession;
import com.unitbv.appfx.user.UserDao;
import com.unitbv.appfx.user.UserService;
import com.unitbv.appfx.user.transport.UserDtoMapper;
import javafx.collections.FXCollections;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import org.mapstruct.factory.Mappers;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

public class MainAppController implements Initializable {

	private final UserDtoMapper userDtoMapper = Mappers.getMapper(UserDtoMapper.class);
	private final UserService userService = new UserService(new UserDao());
	@FXML
	Label balanceLabel;

	@FXML
	Label amountLabel;

	@FXML
	Button payInvoice;

	@FXML
	Button logout;


	@FXML
	Label callCenterLabel;

	@FXML
	Label emergencyPhoneLabel;

	@FXML
	Label userDisplayLabel;

	@FXML
	Button editAccount;

	@FXML
	TableView<Invoice> invoiceTableView;

	@FXML
	public TableColumn<Invoice, String> invoiceNumber;
	@FXML
	public TableColumn<Invoice, Float> amount;
	@FXML
	public TableColumn<Invoice, Invoice.Status> status;
	@FXML
	public TableColumn<Invoice, LocalDate> createDate;
	@FXML
	public TableColumn<Invoice, LocalDate> dueDate;


	private final File file = new File("classpath:emergency.jpg");
	private final Image image = new Image(file.toURI().toString());
	@FXML
	ImageView imageView = new ImageView(image);

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		var email = UserSession.getInstance().getUserEmail();
		userDisplayLabel.setText(email);

		setTableViewBasedOnUserData(email);
	}

	@SneakyThrows
	public void switchToLogin(ActionEvent event) {

		//logout user
		UserSession.getInstance().cleanUserSession();

		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText("Session Account Logout");
		alert.setContentText("Operation Successful!");
		alert.showAndWait();

		Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("login.fxml")));
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	@SneakyThrows
	public void switchToAccount(ActionEvent event) {
		Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("account.fxml")));
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void payInvoice(ActionEvent event){
		if (invoiceTableView.getSelectionModel().getSelectedItem() != null) {
			Invoice selectedInvoice = invoiceTableView.getSelectionModel().getSelectedItem();
			var user = userService.findByEmail(UserSession.getInstance().getUserEmail()).orElseThrow();
			user.getInvoices().stream().filter(invoice -> invoice.getId().equals(selectedInvoice.getId())).findFirst().ifPresent(invoice -> invoice.setStatus(Invoice.Status.PAID));
			userService.saveOrUpdate(user.getEmail(), user);

			var amountToPay= (Long) user.getInvoices().stream().filter(invoice -> invoice.getStatus() == Invoice.Status.UNPAID).mapToLong(Invoice::getAmount).sum();
			amountLabel.setText(amountToPay.toString());

			invoiceTableView.refresh();
		}

	}

	private void setTableViewBasedOnUserData(String email) {
		var user = userService.findByEmail(email).orElseThrow();
		var invoices= user.getInvoices();

		var amountToPay= (Long) user.getInvoices().stream().filter(invoice -> invoice.getStatus() == Invoice.Status.UNPAID).mapToLong(Invoice::getAmount).sum();
		amountLabel.setText(amountToPay.toString());

		invoiceNumber.setCellValueFactory(new PropertyValueFactory<>("Id"));
		amount.setCellValueFactory(new PropertyValueFactory<>("Amount"));
		status.setCellValueFactory(new PropertyValueFactory<>("Status"));
		createDate.setCellValueFactory(new PropertyValueFactory<>("CreateDate"));
		dueDate.setCellValueFactory(new PropertyValueFactory<>("DueDate"));
		SortedList<Invoice> sortedList = new SortedList<>(FXCollections.observableList(List.copyOf(invoices)));
		sortedList.comparatorProperty().bind(invoiceTableView.comparatorProperty());
		invoiceTableView.setItems(sortedList);
		invoiceTableView.getSortOrder().add(dueDate);
		invoiceTableView.sort();

		invoiceTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			//Check whether item is selected and enable/disable pay invoice button based on selected invoice status
			if (invoiceTableView.getSelectionModel().getSelectedItem() != null) {
				Invoice selectedInvoice = invoiceTableView.getSelectionModel().getSelectedItem();
				payInvoice.setDisable(!selectedInvoice.getStatus().equals(Invoice.Status.UNPAID));

				//use this to get the cell value clicked in the table
//					TableView.TableViewSelectionModel<Invoice> selectionModel = invoiceTableView.getSelectionModel();
//					ObservableList<TablePosition> selectedCells = selectionModel.getSelectedCells();
//					TablePosition tablePosition = selectedCells.get(0);
//					Object val = tablePosition.getTableColumn().getCellData(newValue);
//					System.out.println("Selected Value " + val);
			}
		});
	}
}
