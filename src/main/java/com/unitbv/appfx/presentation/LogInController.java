package com.unitbv.appfx.presentation;

import com.unitbv.appfx.session.UserSession;
import com.unitbv.appfx.user.User;
import com.unitbv.appfx.user.UserDao;
import com.unitbv.appfx.user.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.java.Log;

import java.io.IOException;
import java.util.Objects;

@Log
public class LogInController {
	public Label welcomeText;

	@FXML
	public TextField emailField;

	@FXML
	public PasswordField passwordField;

	@FXML
	Button prefill;


	private final UserService userService = new UserService(new UserDao());


	public void prefill(ActionEvent event) throws IOException {
		emailField.setText("alina.tataru@gmail.com");
		passwordField.setText("dd");
	}

	public void login(ActionEvent event) throws IOException {
//		var textNodes = root.getChildrenUnmodifiable().stream().filter(node -> node instanceof TextField);
//		var userTextFieldValue = textNodes.filter(node -> node.getId().equals("user")).findFirst().orElseThrow().getAccessibleText();
//		var passwordTextFieldValue = textNodes.filter(node -> node.getId().equals("password")).findFirst().orElseThrow().getAccessibleText();

		var user = userService.findByEmailAndPassword(emailField.getText(), passwordField.getText());

		log.info(String.valueOf(user));
		user.ifPresent(retrievedUser -> switchToApp(event, retrievedUser));
	}

	public void switchToCreate(ActionEvent event) throws IOException{

		Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("register.fxml")));
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	@SneakyThrows
	public void switchToApp(ActionEvent event, User user) {

		//setting up the session with the login user
		UserSession.getInstance(user.getEmail());

		Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("app.fxml")));
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
