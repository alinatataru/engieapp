package com.unitbv.appfx.presentation;

import com.unitbv.appfx.session.UserSession;
import com.unitbv.appfx.user.User;
import com.unitbv.appfx.user.UserDao;
import com.unitbv.appfx.user.UserService;
import com.unitbv.appfx.user.transport.UserDtoMapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import org.mapstruct.factory.Mappers;

import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

@Log
public class AccountEditController implements Initializable {
	@FXML
	Button cancel;
	@FXML
	Button saveChanges;

	@FXML
	TextField firstName;
	@FXML
	Label errorFirstNameLabel;
	@FXML
	TextField lastName;
	@FXML
	Label errorLastNameLabel;
	@FXML
	TextField email;
	@FXML
	Label errorEmailLabel;
	@FXML
	TextField password;
	@FXML
	Label errorPasswordLabel;
	@FXML
	TextField phoneNumber;
	@FXML
	Label errorPhoneNumberLabel;


	private final UserDtoMapper userDtoMapper = Mappers.getMapper(UserDtoMapper.class);
	private final UserService userService = new UserService(new UserDao());

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		var loginUserEmail = Optional.ofNullable(UserSession.getInstance(null).getUserEmail()).orElseThrow();
		var user = userService.findByEmail(loginUserEmail).orElseThrow();
		log.fine("User is " + user);

		firstName.setText(user.getFirstName());
		lastName.setText(user.getLastName());
		email.setText(user.getEmail());
		password.setText(user.getPassword());
		phoneNumber.setText(user.getPhoneNumber());
	}

	public void updateUser(ActionEvent event) {
		var currentUserEmail = UserSession.getInstance(null).getUserEmail();
		var updatedUser = User.builder()
				.firstName(firstName.getText())
				.lastName(lastName.getText())
				.phoneNumber(phoneNumber.getText())
				.email(email.getText())
				.password(password.getText())
		.build();

		var persistedUser = userService.saveOrUpdate(currentUserEmail, updatedUser);

		UserSession.getInstance().cleanUserSession();
		UserSession.getInstance(persistedUser.getEmail());

		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText("Update Account");
		alert.setContentText("Operation Successful!");
		alert.showAndWait();

		this.switchToMainApp(event);
	}

	@SneakyThrows
	public void switchToMainApp(ActionEvent event) {
		Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("app.fxml")));
		Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
