package com.unitbv.appfx.user;

import com.unitbv.appfx.config.persistence.GenericDao;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.ParameterExpression;
import jakarta.persistence.criteria.Root;
import lombok.extern.java.Log;

import java.util.Optional;

@Log
public class UserDao extends GenericDao<User> {

	private final EntityManager entityManager;

	public UserDao() {
		super(User.class);
		this.entityManager = super.getEntityManager();
	}

	public User find(String email) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);

		Root<User> root = query.from(User.class);
		ParameterExpression<String> paramName = criteriaBuilder.parameter(String.class);
		query.select(root).where(criteriaBuilder.equal(root.get("email"), paramName));
		TypedQuery<User> typedQuery = entityManager.createQuery(query);
		typedQuery.setParameter(paramName, email);

		return typedQuery.getSingleResult();
	}

	public Optional<User> findByIdIgnoreCaseAndPassword(String email, String password) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteria = criteriaBuilder.createQuery(User.class);
		Root<User> userCriteria = criteria.from(User.class);
		criteria.select(userCriteria)
				.where(criteriaBuilder.equal(criteriaBuilder.lower(userCriteria.get("email")), email.toLowerCase()), criteriaBuilder.equal(userCriteria.get(
						"password"), password));
		return entityManager.createQuery(criteria)
				.getResultList()
				.stream()
				.findFirst();
	}

	public User saveOrUpdate(String email, User user) {
		return Optional.ofNullable(find(email))
				.map(existingUser -> {
					existingUser.setEmail(user.getEmail());
					existingUser.setPassword(user.getPassword());
					existingUser.setFirstName(user.getFirstName());
					existingUser.setLastName(user.getLastName());
					existingUser.setPhoneNumber(user.getPhoneNumber());
					existingUser.setAddresses(user.getAddresses());
					existingUser.setInvoices(user.getInvoices());
					super.update(existingUser);
					return existingUser;
				}).orElseGet(() -> {
					super.create(user);
					return user;
				});
	}


}
