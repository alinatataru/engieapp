package com.unitbv.appfx.user;

import com.unitbv.appfx.address.Address;
import com.unitbv.appfx.invoice.Invoice;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


@Entity
@Getter
@Setter
@Table(name = "tab_users")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="User.findById", query = "SELECT e FROM User e WHERE e.email=:email")
public class User {
	@Id
	@Column(name = "email", nullable = false, length = 256)
	private String email;

	@Column(name = "first_name", length = 64)
	private String firstName;

	@Column(name = "last_name", length = 64)
	private String lastName;

	@Column(name = "phone_number", length = 16)
	private String phoneNumber;

	@Column(name = "password", nullable = false, length = 256)
	private String password;

	@Builder.Default
	@OneToMany(mappedBy = "user", orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Set<Invoice> invoices = new HashSet<>();

	@Builder.Default
	@OneToMany(mappedBy = "user", orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Set<Address> addresses = new HashSet<>();

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return email != null && Objects.equals(email, user.email);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" +
				"email = " + email + ", " +
				"firstName = " + firstName + ", " +
				"lastName = " + lastName + ", " +
				"phoneNumber = " + phoneNumber + ")";
	}
}