package com.unitbv.appfx.user.transport;

import com.unitbv.appfx.address.transport.AddressDto;
import com.unitbv.appfx.invoice.Invoice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {


	private String email;

	private String firstName;

	private String lastName;

	private String phoneNumber;

	private String password;

	private Set<Invoice> invoices;

	private Set<AddressDto> addresses;

}