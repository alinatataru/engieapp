package com.unitbv.appfx.user.transport;

import com.unitbv.appfx.address.transport.AddressDtoMapper;
import com.unitbv.appfx.user.User;
import org.mapstruct.Mapper;

@Mapper(uses = AddressDtoMapper.class)
public interface UserDtoMapper {
	UserDto to(User user);
	User from(UserDto userDto);
}
