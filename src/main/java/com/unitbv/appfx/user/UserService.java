package com.unitbv.appfx.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.util.Optional;

@Log
@RequiredArgsConstructor
public class UserService {

	private final UserDao userDao;

	public Optional<User> findByEmailAndPassword(String email, String password) {
		return userDao.findByIdIgnoreCaseAndPassword(email, password);
	}

	public Optional<User> findByEmail(String email){
		return Optional.ofNullable(userDao.find(email));
	}

	public User save(User user){
		 userDao.create(user);
		 return user;
	}

	public User update(User user){
		userDao.update(user);
		return findByEmail(user.getEmail()).orElseThrow();
	}

	public User saveOrUpdate(String email, User user){
		return userDao.saveOrUpdate(email, user);
	}
}
