package com.unitbv.appfx.user;


import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

import java.util.Optional;

public class UserRepository {

    public UserRepository(EntityManager em) {
        this.em = em;
    }

    private final EntityManager em;




    public User saveOrUpdate(User user) {
        return findById(user.getEmail())
                .map(existingUser -> {
                    existingUser.setFirstName(user.getFirstName());
                    existingUser.setLastName(user.getLastName());
                    existingUser.setPhoneNumber(user.getPhoneNumber());
                    existingUser.setInvoices(user.getInvoices());
                    existingUser.setPassword(user.getPassword());
                    flush();
                    return existingUser;
                })
                .orElseGet(() -> {
                    em.persist(user);
                    flush();
                    return findById(user.getEmail()).stream()
                                                    .findFirst()
                                                    .orElseThrow(() -> new RuntimeException("Unable to persist user " +
                                                            "with id " + user.getEmail()));
                });
    }

    public Optional<User> findById(String email) {
//        return Optional.ofNullable(em.find(User.class, email));
        TypedQuery<User> typedQuery = em.createNamedQuery("User.findById", User.class);
        typedQuery.setParameter("email", email);
        return Optional.ofNullable(typedQuery.getSingleResult());
    }

    public Optional<User> findByIdIgnoreCaseAndPassword(String email, String password) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> userCriteria = criteria.from(User.class);
        criteria.select(userCriteria)
                .where(cb.equal(cb.lower(userCriteria.get("email")), email.toLowerCase()), cb.equal(userCriteria.get(
                        "password"), password));
        return em.createQuery(criteria)
                 .getResultList()
                 .stream()
                 .findFirst();
    }

    public boolean delete(User user) {
        user = em.merge(user);
        em.remove(user);
        return true;
    }

    private void flush() {
        em.flush();
    }

}
