package com.unitbv.appfx.config.persistence;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.Map;

// Getting the EntityManagerFactory from a singleton in the EntityManagerFactorySingleton class
public class EntityManagerFactorySingleton {

    public static final boolean DEBUG = true;

    private static final EntityManagerFactorySingleton singleton = new EntityManagerFactorySingleton();

    protected EntityManagerFactory emf;

    public static EntityManagerFactorySingleton getInstance() {

        return singleton;
    }

    private EntityManagerFactorySingleton() {
    }

    public EntityManagerFactory getEntityManagerFactory(Map<?, ?> properties) {

        if (emf == null)
            createEntityManagerFactory(properties);
        return emf;
    }

    public void closeEntityManagerFactory() {

        if (emf != null) {
            emf.close();
            emf = null;
            if (DEBUG)
                System.out.println("n*** Persistence finished at " + new java.util.Date());
        }
    }

    protected void createEntityManagerFactory(Map<?, ?> properties) {

        this.emf = Persistence.createEntityManagerFactory("default", properties);
        if (DEBUG)
            System.out.println("n*** Persistence started at " + new java.util.Date());
    }
}
