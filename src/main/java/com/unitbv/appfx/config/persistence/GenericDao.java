package com.unitbv.appfx.config.persistence;


import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaQuery;
import lombok.extern.java.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

@Log
public abstract class GenericDao<T> {

	private Class<T> entityClass;

	public GenericDao(Class<T> entityClass){
		this.entityClass = entityClass;
	}
	public  EntityManager getEntityManager(){
	return 	EntityManagerFactorySingleton.getInstance().getEntityManagerFactory(new HashMap<>()).createEntityManager();
	}

	public void create(T entity){
		EntityManager entityManager = getEntityManager();
		try{
			entityManager.getTransaction().begin();
			entityManager.persist(entity);
			entityManager.getTransaction().commit();

		}catch (Exception exception){
			log.log(Level.SEVERE,"Error during create", exception);
			entityManager.getTransaction().rollback();
		}finally {
			entityManager.close();
		}
	}

	public void update(T entity){
		EntityManager entityManager = getEntityManager();
		try{
			entityManager.getTransaction().begin();
			entityManager.merge(entity);
			entityManager.getTransaction().commit();

		}catch (Exception exception){
			log.log(Level.SEVERE,"Error during update", exception);
			entityManager.getTransaction().rollback();
		}finally {
			entityManager.close();
		}
	}

	public void remove(T entity, int entityId){
		EntityManager entityManager = getEntityManager();
		try{
			entityManager.getTransaction().begin();
			entityManager.remove((T) entityManager.find(this.entityClass, entityId));
			entityManager.getTransaction().commit();
		}catch(Exception exception){
			log.log(java.util.logging.Level.SEVERE, "Error during remove", exception);
			entityManager.getTransaction().rollback();
		}finally {
			entityManager.close();
		}
	}
	public Optional<T> find(int id){
		EntityManager entityManager = getEntityManager();
		try{
			return Optional.ofNullable((T) entityManager.find(this.entityClass, id));
		}catch(Exception exception){
			log.log(java.util.logging.Level.SEVERE, "Error during find", exception);
			entityManager.getTransaction().rollback();
		}finally {
			entityManager.close();
		}
		return Optional.empty();
	}

	public List<T> findAll(){
		EntityManager entityManager = getEntityManager();
		try{
			CriteriaQuery<Object> criteriaQuery = entityManager.getCriteriaBuilder().createQuery();
			criteriaQuery.select(criteriaQuery.from(entityClass));
			return (List<T>) entityManager.createQuery(criteriaQuery).getResultList();
		} catch(Exception exception){
			log.log(java.util.logging.Level.SEVERE, "Error during find", exception);
			entityManager.getTransaction().rollback();
		}finally {
			entityManager.close();
		}
		return null;
	}


}
