package com.unitbv.appfx.config.persistence;

import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.SessionEvent;
import org.eclipse.persistence.sessions.SessionEventAdapter;
import org.eclipse.persistence.sessions.UnitOfWork;

public class ImportSQL implements SessionCustomizer {

	private void importSql(UnitOfWork unitOfWork, String fileName) {
		// Open file
		// Execute each line, e.g.,
		// unitOfWork.executeNonSelectingSQL("select 1 from dual");
	}

	@Override
	public void customize(Session session) throws Exception {
		session.getEventManager().addListener(new SessionEventAdapter() {
			@Override
			public void postLogin(SessionEvent event) {
				String fileName = (String) event.getSession().getProperty("import.sql.file");
				UnitOfWork unitOfWork = event.getSession().acquireUnitOfWork();
				importSql(unitOfWork, fileName);
				unitOfWork.commit();
			}
		});
	}
}