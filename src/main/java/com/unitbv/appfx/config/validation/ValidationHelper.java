package com.unitbv.appfx.config.validation;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;

import java.util.function.Predicate;
import java.util.regex.Pattern;

@Log
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationHelper {

	public static final String EMAIL_NOT_VALID = "Email not valid!";
	public static final String PHONE_NUMBER_NOT_VALID = "Phone number not valid!";
	public static final String PASSWORD_NOT_VALID = "Password not valid!";
	public static final String FIRST_NAME_NOT_VALID = "First name not valid!";
	public static final String LAST_NAME_NOT_VALID = "Last name not valid!";
	public static final String COD_INST_NOT_VALID = "Cod inst not valid!";
	public static final String COD_CONTRACT_NOT_VALID = "Cod contract not valid!";
	public static final String COUNTRY_NOT_VALID = "Country not valid!";
	public static final String COUNTY_NOT_VALID = "County not valid!";
	public static final String CITY_NOT_VALID = "City not valid!";
	public static final String STREET_NUMBER_NOT_VALID = "Street number not valid!";
	public static final String POSTAL_CODE_NOT_VALID = "Postal code not valid!";

	public static boolean applyValidation(TextField textField, Label errorLabel, String errorMessage, Predicate<String> validation) {
		errorLabel.setVisible(false);
		if (!validation.test(textField.getText())) {
			errorLabel.setText(errorMessage);
			errorLabel.setVisible(true);
			log.fine(errorMessage);
			return false;
		}
		return true;
	}

	public static Predicate<String> isEmailValid = email -> {
		String regexEmail = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		Pattern pattern = Pattern.compile(regexEmail);
		return email != null && pattern.matcher(email).matches();
	};

	public static Predicate<String> isPasswordValid = password -> {
		//	Minimum eight characters, at least one letter, one number and one special character
		String regexPassword = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$";
		Pattern pattern = Pattern.compile(regexPassword);
		return password != null && pattern.matcher(password).matches();
	};

	public static Predicate<String> isPhoneNumberValid = phoneNumber -> {
		String regexNumber = "^(\\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\\s|\\.|\\-)?([0-9]{3}(\\s|\\.|\\-|)){2}$";
		Pattern pattern = Pattern.compile(regexNumber);
		return phoneNumber != null && pattern.matcher(phoneNumber).matches();
	};

	public static Predicate<String> isNotEmpty = text -> !text.isEmpty();

}
