package com.unitbv.appfx.invoice;

import com.unitbv.appfx.user.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Setter
@Getter
@Entity
@ToString
@Table(name = "tab_invoices")
public class Invoice {
	@Id
	@Column(name = "invoice_number", nullable = false, length = 12)
	private String id;

	@Column(name = "create_date")
	private LocalDate createDate;

	@Column(name = "due_date")
	private LocalDate dueDate;

	@Column(name = "amount")
	private Integer amount;

	@Column(name = "amount_paid")
	private Integer amountPaid;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;

	@ToString.Exclude
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	public enum Status {
		UNPAID,
		PAID
	}
}