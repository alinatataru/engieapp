package com.unitbv.appfx.invoice;

import com.unitbv.appfx.config.persistence.GenericDao;
import jakarta.persistence.EntityManager;
import lombok.extern.java.Log;

@Log
public class InvoiceDao extends GenericDao<Invoice> {
	private final EntityManager entityManager;

	public InvoiceDao() {
		super(Invoice.class);
		this.entityManager = super.getEntityManager();
	}

}
