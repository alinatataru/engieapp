package com.unitbv.appfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import org.kordamp.bootstrapfx.BootstrapFX;

import java.util.logging.Level;

@Log
public class EngieApplication extends Application {

	@Override
	public void start(Stage stage) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(EngieApplication.class.getResource("presentation/login.fxml"));
			Scene scene = new Scene(fxmlLoader.load(), 600, 400);
			scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());
			stage.setScene(scene);
			stage.setTitle("Engie");
			Image image = new Image("engie.jpg");
			stage.getIcons().add(image);
			stage.show();
		} catch(Exception exception){
			log.log(Level.SEVERE,"Error during start", exception);
		}
	}

	public static void main(String[] args) {
		launch();
	}
}
