package com.unitbv.appfx.address;

import com.unitbv.appfx.config.persistence.GenericDao;
import jakarta.persistence.EntityManager;
import lombok.extern.java.Log;

@Log
public class AddressDao extends GenericDao<Address> {
	private final EntityManager entityManager;

	public AddressDao(Class<Address> entityClass) {
		super(entityClass);
		this.entityManager = super.getEntityManager();
	}
}
