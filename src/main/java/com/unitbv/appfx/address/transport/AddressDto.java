package com.unitbv.appfx.address.transport;

import com.unitbv.appfx.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {

	private UUID id;

	private String country;

	private String county;

	private String city;

	private String streetNumber;

	private String postalCode;

	private User user;

}