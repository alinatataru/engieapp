package com.unitbv.appfx.address.transport;

import com.unitbv.appfx.address.Address;
import org.mapstruct.Mapper;

@Mapper
public interface AddressDtoMapper {
	AddressDto to(Address address);
	Address from(AddressDto addressDto);
}
