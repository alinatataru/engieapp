package com.unitbv.appfx.address;

import com.unitbv.appfx.user.User;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tab_addresses")
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	@Column(name = "id", nullable = false)
	private UUID id;

	@Column(name = "country", length = 32)
	private String country;

	@Column(name = "county", length = 32)
	private String county;

	@Column(name = "city", length = 32)
	private String city;

	@Column(name = "street_number", length = 4)
	private String streetNumber;

	@Column(name = "postal_code", length = 16)
	private String postalCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinTable(name = "tab_users_addresses",
			joinColumns = @JoinColumn(name = "address_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id"))
	private User user;

}