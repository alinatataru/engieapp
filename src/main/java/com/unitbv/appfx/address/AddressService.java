package com.unitbv.appfx.address;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

@Log
@RequiredArgsConstructor
public class AddressService {

	private final AddressDao addressDao;


	public Address save(Address address){
		 addressDao.create(address);
		 return address;
	}
}
