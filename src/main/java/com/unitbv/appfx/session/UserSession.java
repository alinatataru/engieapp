package com.unitbv.appfx.session;

import lombok.Getter;
import lombok.ToString;


@Getter
@ToString
public final class UserSession {

	private static UserSession instance;

	private final String userEmail;

	private UserSession(String userEmail) {
		this.userEmail = userEmail;
	}

	public static UserSession getInstance(String userEmail) {
		if(instance == null) {
			instance = new UserSession(userEmail);
		}
		return instance;
	}

	public static UserSession getInstance() {
		return instance;
	}

	public void cleanUserSession() {
		instance = null;
	}
}
