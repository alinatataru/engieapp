package com.unitbv.appfx.model;

import com.unitbv.appfx.config.persistence.EntityManagerFactorySingleton;
import com.unitbv.appfx.user.UserRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.HashMap;
import java.util.Map;

import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_PASSWORD;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_URL;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_USER;

@Testcontainers
class UserRepositoryTest {

    DockerImageName myImage =
            DockerImageName.parse("postgres:14-alpine")
                           .asCompatibleSubstituteFor("postgresql");

    @Container
    PostgreSQLContainer<?> postgreSQLContainer =
            new PostgreSQLContainer<>(myImage);

    EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;
    UserRepository repository;

    @BeforeEach
    public void setUp() {
        Map<String,String> properties = new HashMap<>();
        properties.put(JDBC_URL, postgreSQLContainer.getJdbcUrl());
        properties.put(JDBC_USER, postgreSQLContainer.getUsername());
        properties.put(JDBC_PASSWORD, postgreSQLContainer.getPassword());


        entityManagerFactory = EntityManagerFactorySingleton.getInstance().getEntityManagerFactory(properties);
        entityManager = entityManagerFactory.createEntityManager();
        repository = new UserRepository(entityManager);
    }




    @Test
    void saveOrUpdate() {
    }

    @Test
    void findById() {
    }

    @Test
    void findByIdIgnoreCaseAndPassword() {
    }

    @Test
    void delete() {
    }
}
