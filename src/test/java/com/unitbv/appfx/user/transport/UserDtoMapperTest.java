package com.unitbv.appfx.user.transport;

import com.unitbv.appfx.address.Address;
import com.unitbv.appfx.address.transport.AddressDto;
import com.unitbv.appfx.user.User;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserDtoMapperTest {

	private final UserDtoMapper userDtoMapper = Mappers.getMapper(UserDtoMapper.class);

	@Test
	void to() {
		//Given
		var address = Address.builder()
				.country("Ro")
				.county("Bv")
				.city("Brasov")
				.streetNumber("5")
				.postalCode("500170")
				.build();
		var user = User.builder()
				.email("@alina")
				.firstName("Alina")
				.lastName("Tataru")
				.phoneNumber("0743265577")
				.password("hgfdK")
				.addresses(Set.of(address))
				.build();
		//When
		UserDto userDto = userDtoMapper.to(user);

		//Then
		assertEquals(user.getEmail(), userDto.getEmail());
		assertEquals(user.getFirstName(), userDto.getFirstName());
		assertEquals(user.getLastName(), userDto.getLastName());
		assertEquals(user.getPhoneNumber(), userDto.getPhoneNumber());
		assertEquals(user.getPassword(), userDto.getPassword());
		assertEquals(user.getAddresses().stream().findFirst().orElseThrow(), address);
	}

	@Test
	void from() {
		//Given
		var addressDto = AddressDto.builder()
				.country("Ro")
				.county("Bv")
				.city("Brasov")
				.streetNumber("5")
				.postalCode("500170")
				.build();
		var userDto = UserDto.builder()
				.email("@alina")
				.firstName("Alina")
				.lastName("Tataru")
				.phoneNumber("0743265577")
				.password("hgfdK")
				.addresses(Set.of(addressDto))
				.build();
		//When
		User user = userDtoMapper.from(userDto);

		//Then
		assertEquals(userDto.getEmail(), user.getEmail());
		assertEquals(userDto.getFirstName(), user.getFirstName());
		assertEquals(userDto.getLastName(), user.getLastName());
		assertEquals(userDto.getPhoneNumber(),user.getPhoneNumber());
		assertEquals(userDto.getPassword(), user.getPassword());
		//TODO sa corectez aici
//		assertEquals(addressDto, user.getAddresses().stream().findFirst().orElseThrow());

	}
}